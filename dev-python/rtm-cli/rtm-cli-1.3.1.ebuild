# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI="5"
DESCRIPTION="RTM-CLI: Remember the Milk Command Line Interface"
HOMEPAGE="http://www.davidwaring.net/projects/rtm.html https://bitbucket.org/dwaring87/rtm-cli/overview"
SRC_URI="https://bitbucket.org/dwaring87/rtm-cli/get/${PV}.tar.bz2 -> ${P}.tar.bz2"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"

TAG_HEAD=c5001a009229
S="${WORKDIR}/dwaring87-rtm-cli-$TAG_HEAD"

src_install() {
	dodoc README
	dobin rtm
}
