# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit multilib

GITHUB_REV=2b6e8d35aeba3a55bd87f6c1a57bf71491029caf
MY_P="libhashab-${GITHUB_REV}"
DESCRIPTION="libgpod extension to make it talk to iPod nano 6th generation"
HOMEPAGE="https://github.com/denydias/libhashab"
SRC_URI="https://github.com/denydias/libhashab/archive/${GITHUB_REV}.zip -> ${MY_P}.zip"

LICENSE="Apple" # upstream has no license, and was reverse-engineered from Apple binaries
SLOT="0"
KEYWORDS="-* ~amd64 ~x86"
IUSE=""

DEPEND=""
# The multilib dep is NOT an error
# this is a 32-bit .so library, as well as a 64-bit .so the wraps calling 
RDEPEND=">=media-libs/libgpod-0.8.3
		sys-libs/glibc[multilib]
		sys-apps/util-linux"
RESTRICT="bindist mirror strip"

S=${WORKDIR}/${MY_P}

src_prepare() {
	LIBDIR_32=/usr/$(get_abi_LIBDIR x86)/libgpod
	sed -i \
		-e "s,/usr/lib/x86_64-linux-gnu/libgpod/libhashab32.so,${LIBDIR_32}/libhashab.so," \
		src/libhashab32_wrapper.c
	sed -i \
		-e "/linux32/s,/usr/lib/x86_64-linux-gnu/libgpod/libhashab32_wrapper,${LIBDIR_32}/libhashab32_wrapper,g" \
		./src/libhashab.c
	sed -i \
		-e '/gcc.*\.c/s,$, $CFLAGS,g' \
		-e '/gcc.*-o/s,$, $LDFLAGS,g' \
		src/build.sh
}

src_compile() {
	if use amd64; then
		cd src && ./build.sh
	fi
}

src_install() {
	LIBDIR_32=/usr/$(get_abi_LIBDIR x86)/libgpod
	exeinto ${LIBDIR_32}
	newexe libhashab32.so libhashab.so
	if use amd64; then
		LIBDIR_64=/usr/$(get_abi_LIBDIR amd64)/libgpod
		cd src
		exeinto ${LIBDIR_32}
		doexe libhashab32_wrapper
		exeinto ${LIBDIR_64}
		doexe libhashab.so
	fi
}
