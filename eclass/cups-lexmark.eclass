# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

#
# Original Author: Robin H. Johnson <robbat2@gentoo.org>
# Purpose: 
#

inherit base

DESCRIPTION="${DESCRIPTION:=Lexmark $MODEL_NAMES drivers}"
LICENSE="${LICENSE:=Lexmark}"
RESTRICT="mirror"
HOMEPAGE="${HOMEPAGE:=http://support.lexmark.com/}"
if [[ -n ${MODEL_CODE} ]]; then
	A_R="PPD-Files-${MODEL_CODE}.tar.Z"
	A_D="PPD-Files-${MODEL_CODE}-${PV}.tar.Z"
	SRC_URI="${SRC_URI:=http://downloads.lexmark.com/downloads/pssd/$A_R -> $A_D}"
fi

IUSE="contone"
DEPEND=""
RDEPEND="net-print/foomatic-db-ppds
		>=net-print/cups-1.4
		net-print/cups-filters"
COMMON_CP="net-print/lexmark-ppd-common"
[[ "$CATEGORY/$PN" != "$COMMON_CP" ]] && RDEPEND="${RDEPEND} ${COMMON_CP}"

CUPS_FILTER_PATH=/usr/libexec/cups/filter
FOOMATIC_SRC=/usr/share/foomatic/db/source
S=${WORKDIR}/ppd_files
SRCDIR=${S}/GlobalPPD_1.4
FILTERS+=( fax-pnh-filter pdftocontone )
QA_FLAGS_IGNORED="${CUPS_FILTER_PATH}/pdftocontone"

EXPORT_FUNCTIONS src_unpack src_prepare src_compile src_install

cups-lexmark_src_unpack() {
	base_src_unpack
	# both foomatic/UTF-8 and foomatic/ISO8859-1 are identical actually
	cd "${S}"/foomatic/UTF-8
	unpack ./foomatic.tar
}

cups-lexmark_src_prepare() {
	for PPD in $(find -name '*.PPD') ; do
		mv "${PPD}" "${PPD/.PPD/.ppd}"
	done
	for PPD in $(find -name '*.ppd') ; do
		#einfo "Fix $PPD"
		sed -i \
			-e "/cupsFilter/s,/usr/lib/cups/filter,$CUPS_FILTER_PATH,g" \
			$PPD || die "Failed to sed"
		# This is somewhat complicated.
		# pdftocontone is a precompiled binary that is used on SuSE/Fedora
		# but in Ubuntu, the upstream installed script removes it from the PPD
		use contone || \
		sed -i \
			-e "/^\*cupsFilter:.*application\/pdf.*pdftocontone/d" \
			$PPD || die "Failed to sed"
	done
}

cups-lexmark_src_compile() {
	for PPD in $(find -name '*.ppd') ; do
		#einfo "Compress $PPD"
		[ -f ${PPD}.gz ] || gzip -9k $PPD
	done
}

cups-lexmark_ppd_src_install() {
	insinto /usr/share/ppd/Lexmark
	for d in "${SRCDIR}" UTF-8 ; do
		[[ -d $d ]] || continue
		for PPD in $(find "$d" -name '*.ppd.gz') ; do
			#einfo "Installing $PPD"
			doins $PPD
		done
	done
	# This file is badly inconsistent between packages
	# So we install our own clean one further down
	rm -f foomatic/UTF-8/driver/Lexmark_PPD.xml
	cd "${S}"/foomatic/UTF-8 || die "Missing dir"
	insinto ${FOOMATIC_SRC}
	for i in driver opt printer ; do
		[[ -d $i ]] || continue
		#einfo "Installing foomatic/$i"
		doins -r $i || die "Missing files in $i"
	done
}

cups-lexmark_common_src_install() {
	exeinto ${CUPS_FILTER_PATH}
	for filter in ${FILTERS[*]} ; do 
		einfo "Installing filter $filter"
		for s in \
			"${FILESDIR}"/$filter \
			"$SRCDIR"/$filter \
			"$SRCDIR"/$(get_libdir)/$filter ; do
			if [[ -f $s ]]; then
				doexe $s
				break
			fi
		done
	done
	insinto ${FOOMATIC_SRC}/driver
	doins foomatic/UTF-8/driver/Lexmark_PPD.xml
}

cups-lexmark_src_install() {
	echo $COMMON_CP
	if [[ "$CATEGORY/$PN" == "${COMMON_CP}" ]]; then
		cups-lexmark_common_src_install
	else
		cups-lexmark_ppd_src_install
	fi
}
