# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

MODULE_AUTHOR=BFAIST
MODULE_VERSION=${PV:0:4}.${PV:5}
inherit perl-module

DESCRIPTION="Provides interface to SlimTimer web service"

#LICENSE="|| ( Artistic GPL-1 GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

# File-Spec
RDEPEND="
dev-perl/DateTime
dev-perl/DateTime-Format-Natural
dev-perl/DateTime-Format-RFC3339
dev-perl/File-Slurp
dev-perl/Getopt-Long-Descriptive
dev-perl/libwww-perl
dev-perl/Moose
dev-perl/MooseX-Declare
dev-perl/MooseX-Method-Signatures
dev-perl/MooseX-Types
dev-perl/YAML-XS
"
DEPEND="${RDEPEND}"

SRC_TEST="do"
