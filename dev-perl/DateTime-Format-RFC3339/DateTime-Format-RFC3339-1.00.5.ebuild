# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-perl/DateTime-Format-ISO8601/DateTime-Format-ISO8601-0.80.0-r1.ebuild,v 1.3 2014/11/30 08:42:18 zlogene Exp $

EAPI=5

MODULE_AUTHOR=IKEGAMI
MODULE_VERSION=v1.0.5
inherit perl-module

DESCRIPTION="Parse and format RFC3339 datetime strings"

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

RDEPEND="dev-perl/DateTime"
DEPEND="${RDEPEND}
	virtual/perl-Module-Build"
# Test::More is stock perl

SRC_TEST=do
