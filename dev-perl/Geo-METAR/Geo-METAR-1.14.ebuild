# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

MODULE_AUTHOR="JZAWODNY"
inherit perl-module

DESCRIPTION="Process Aviation Weather (METAR) Data"

IUSE="test"

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~amd64 ~ppc ~x86"
mydoc="HACKING TODO examples/*.pl"
SRC_TEST="do"
