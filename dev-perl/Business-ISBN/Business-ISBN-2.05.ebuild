# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# This ebuild generated by g-cpan 0.16.4

EAPI="2"

MODULE_AUTHOR="BDFOY"


inherit perl-module

DESCRIPTION="Work with ISBN as objects"

LICENSE="|| ( Artistic GPL-1 GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="dev-perl/URI
	>=dev-perl/Business-ISBN-Data-20081208
	dev-lang/perl"
