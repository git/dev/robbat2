# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

MODULE_AUTHOR="DJK"
inherit perl-module

DESCRIPTION="Decode METAR and TAF strings "

IUSE="test"

SLOT="0"
LICENSE="|| ( Artistic GPL-2 )"
KEYWORDS="~amd64 ~ppc ~x86"
mydoc="Changes README examples/*"
SRC_TEST="do"
