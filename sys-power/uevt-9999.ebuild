# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
EGIT_REPO_URI="git://git.sleipnir.fr/pub/git/uevt"
EGIT_BRANCH="vala012"
VALA_VERSION=0.12

inherit flag-o-matic toolchain-funcs autotools git

DESCRIPTION="A lightweight, desktop-independant daemon for disks mounting and power managing"
HOMEPAGE="http://elentir.sleipnir.fr/ http://git.sleipnir.fr/uevt/"
SRC_URI=""

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

COMMON_DEPEND=">=dev-libs/glib-2.26
	x11-libs/gtk+:2
	>=x11-libs/libnotify-0.4.5"
RDEPEND="${COMMON_DEPEND}
	sys-fs/udisks
	sys-power/upower"
DEPEND="${COMMON_DEPEND}
	dev-lang/vala:${VALA_VERSION}
	dev-util/intltool
	dev-util/pkgconfig
	sys-devel/gettext"

src_prepare() {
	git_src_prepare
	./autogen.sh
	eautoreconf
	sed -i -e "s:valac:valac-${VALA_VERSION}:" configure || die
}

src_configure() {
	#append-cppflags "$(pkg-config --cflags-only-I gtk+-2.0)"

	econf \
		--disable-dependency-tracking
}

src_install() {
	emake DESTDIR="${D}" install || die
	dodoc AUTHORS ChangeLog README
}
