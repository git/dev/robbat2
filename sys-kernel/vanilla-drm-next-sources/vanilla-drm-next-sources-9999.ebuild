# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

ETYPE="sources"
K_SECURITY_UNSUPPORTED="1"
K_DEBLOB_AVAILABLE=0
# If these are not set, you will get weird behavior from kernel-2, due to the
# huge $PV that is used otherwise.
#CKV='2.6.99'

RESTRICT="binchecks strip primaryuri mirror"

EGIT_REPO_URI="git://git.kernel.org/pub/scm/linux/kernel/git/airlied/drm-2.6.git"
EGIT_BRANCH="drm-next"

inherit kernel-2 git
detect_version

DESCRIPTION="Full sources for the Linux kernel drm-next branch"
HOMEPAGE="http://www.kernel.org"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

pkg_setup(){
	kernel-2_pkg_setup
}

src_install() {
	sed -e 's/^EXTRAVERSION =$/EXTRAVERSION = -drm-next/' "${S}"/Makefile -i || die "sed failed"
	kernel-2_src_install
}
