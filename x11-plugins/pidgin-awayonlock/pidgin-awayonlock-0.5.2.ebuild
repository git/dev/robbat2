# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
MY_PN="${PN/pidgin-}"
MY_P="${MY_PN}-${PV}"

inherit cmake-utils

DESCRIPTION="Away-on-Lock change your Pidgin status when the screensaver gets activated"
HOMEPAGE="http://costela.net/projects/awayonlock/"
SRC_URI="http://costela.net/files/${MY_P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="net-im/pidgin
		 dev-libs/dbus-glib"

DEPEND="${RDEPEND}
	dev-util/pkgconfig"


S="${WORKDIR}/${MY_P}"

src_install() {
	cmake-utils_src_install
	dodoc AUTHORS
	insinto /usr/share/doc/${PF}
	doins utils/mimic_gnome-screensaver.py
}
