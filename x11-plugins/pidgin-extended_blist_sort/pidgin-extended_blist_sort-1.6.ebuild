# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
PROJ_PN="p-extblistsort"
MY_PN="${PN/pidgin-}"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="Plugin allows you to sort your buddy list more flexible than Pidgin does"
HOMEPAGE="http://sourceforge.net/projects/p-extblistsort/"
SRC_URI="mirror://sourceforge/${PROJ_PN}/${MY_P}.tar.gz"
#http://sourceforge.net/projects/p-extblistsort/files/p-extblistsort/1.6/extended_blist_sort-1.6.tar.gz/download"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="net-im/pidgin[gtk]
		 dev-libs/glib"

DEPEND="${RDEPEND}
	dev-util/pkgconfig"

S="${WORKDIR}/${MY_P}"

src_install() {
	emake install DESTDIR="${D}" || die "emake install failed"
	dodoc README AUTHORS VERSION
}
