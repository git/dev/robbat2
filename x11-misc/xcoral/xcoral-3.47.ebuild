# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
inherit eutils multilib

DESCRIPTION="multiwindow mouse-based text editor for Unix and X Window System"
HOMEPAGE="http://xcoral.free.fr/"
SRC_URI="http://xcoral.free.fr/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_configure() {
	econf --with-x || die
}

XCORAL_SHAREDIR=/usr/share/xcoral

src_compile() {
	local makeopts="XCORALLIBDIR=\\\"${XCORAL_SHAREDIR}\\\""
	# This avoids the parallel build weirdness.
	# splitting the by build into 3 parts.
	pushd "${S}/Smac"
	# this is the only bit that isn't parallel safe itself
	emake -j1 word.h y.tab.c ${makeopts} || die
	# But we need to build the rest of the dir else it interacts weirdly with
	# parallel.
	emake all ${makeopts} || die
	popd
	# Last part.
	emake ${makeopts} || die
}

src_install() {
	dobin xcoral
	dodoc README Doc/*pdf
	insinto ${XCORAL_SHAREDIR}
	doins SmacLib/*
}
