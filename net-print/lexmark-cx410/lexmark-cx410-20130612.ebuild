# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit base

DESCRIPTION="Lexmark CX410 foomatic drivers"
#HOMEPAGE="http://support.lexmark.com/index?productCode=LEXMARK_CX410&page=product&locale=EN&userlocale=EN_US#1"
HOMEPAGE="http://support.lexmark.com/index?page=content&productCode=LEXMARK_C736N&locale=en&segment=DOWNLOAD&oslocale=es_ES&actp=RECOMMEND&userlocale=EN&id=DR20047"
MODELS=( 
LMAAT # Lexmark T640, Lexmark T642, Lexmark T644
LMABS # Lexmark C936
LMABT # Lexmark X940e, Lexmark X945e
LMACA # Lexmark C546, Lexmark C540, Lexmark C543, Lexmark C544
LMACB # Lexmark X543, Lexmark X544, Lexmark X546
LMACC # Lexmark C734n, Lexmark C736n
LMACE # Lexmark X734de, Lexmark X736de, Lexmark X738de
LMACG
LMACH # Lexmark E360
LMACI # Lexmark E450dn, Lexmark E460, Lexmark E462
LMACJ # Lexmark X264, Lexmark X363, Lexmark X364
LMACK
LMACL # Lexmark T650, Lexmark T652, Lexmark T654, Lexmark T656
LMACO # Lexmark X203n, Lexmark X204n
LMACP # Lexmark W850
LMACQ # Lexmark X864, Lexmark X860, Lexmark X862
LMACS
LMAD4
)
SRC_URI=""
for m in ${MODELS[*]} ; do
SRC_URI="${SRC_URI} http://downloads.lexmark.com/downloads/pssd/PPD-Files-${m}.tar.Z"
done

LICENSE="Lexmark"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="net-print/foomatic-db-ppds
		>=net-print/cups-1.4"
DEPEND=""

S="${WORKDIR}/ppd_files"
CUPS_FILTER_PATH=/usr/libexec/cups/filter/
SRCDIR=GlobalPPD_1.4
PPD=$SRCDIR/Lexmark_CX410_Series.ppd
FILTER=$SRCDIR/fax-pnh-filter

src_unpack() {
	base_src_unpack
	# both foomatic/UTF-8 and foomatic/ISO8859-1 are identical actually
	cd "${S}"/foomatic/UTF-8
	pwd
	unpack ./foomatic.tar
}

src_prepare() {
	sed -i \
		-e "/cupsFilter/s,/usr/lib/cups/filter/,$CUPS_FILTER_PATH,g" \
		$PPD || die "Failed to sed"
}

src_compile() {
	[ -f $PPD.gz ] || gzip -9k $PPD
}

src_install() {
	insinto /usr/share/ppd/Lexmark
	doins ${PPD}.gz
	exeinto /usr/libexec/cups/filter
	doexe ${FILTER}
	cd "${S}"/foomatic/UTF-8
	foomatic_src=/usr/share/foomatic/db/source
	for i in driver opt printer ; do
		insinto ${foomatic_src}/$i
		doins $i/* || die "Missing files in $i"
	done
}
