# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

# any of LMADH/LMADG/LMADE for the latest version of pdfcontone
MODEL_CODE=LMADH

inherit cups-lexmark
DESCRIPTION="CUPS files common to Lexmark drivers"
SLOT="0"
KEYWORDS="~amd64 ~x86"

src_prepare() {
	cups-lexmark_src_prepare
	cd "${S2}"
	sed -i \
		-e 's/GEYMYUSERNAME/GE[YT]MYUSERNAME/' \
		"$SRCDIR"/fax-pnh-filter \
		|| die "sed of fax-pnh-filter failed"
	sed -i.orig \
		-e '/<url>/,/<\/url>/d' \
		-e '/<\/name>/a<url>http://www.lexmark.com</url>' \
		"${S}"/foomatic/UTF-8/driver/Lexmark_PPD.xml \
		|| die "sed of Lexmark_PPD.xml failed"
}
