# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

MODEL_CODE=${PN//lexmark-ppd-}
MODEL_NAMES="X651de, X652de, X654de, X656de, X658de"
inherit cups-lexmark

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
